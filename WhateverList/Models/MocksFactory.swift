//
//  MocksFactory.swift
//  WhateverList
//
//  Created by Natalia González Junco on 8/09/21.
//

import Foundation

struct MocksFactory {
    static func getTaskMocks(size: Int) -> [Task] {
        (0...size).map { _ in Task.getMock() }
    }
    
}

fileprivate extension Task {
    static func getMock() -> Task {
        
        let task = Task(name: "Task",
                        description: "Descripcion",
                        status: TaskStatus.getMock(),
                        initialDate: Date(),
                        finalDate: nil,
                        subTasks: nil)
        return task
    }
}

fileprivate extension TaskStatus {
    static func getMock() -> TaskStatus {
        return TaskStatus.allCases.randomElement() ?? TaskStatus.completed
    }
}
