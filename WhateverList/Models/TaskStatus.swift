//
//  TaskStatus.swift
//  WhateverList
//
//  Created by Natalia González Junco on 8/09/21.
//

import Foundation

enum TaskStatus: CaseIterable {
    case completed
    case inProcess
    case notStarted
    
    var cellImageName: String {
        switch self {
        case .completed:
            return "paperclip.circle"
        case .inProcess:
            return "book.circle.fill"
        case .notStarted:
            return "person.crop.circle"
        }
    }
    
    var cellStatusColorName: String {
        switch self {
        case .completed:
            return "g"
        case .inProcess:
            return ""
        case .notStarted:
            return ""
        }
    }
    
}
