//
//  WorkListTodayCell.swift
//  PairProgramming
//
//  Created by Natalia González Junco on 6/09/21.
//

import UIKit

class WorkListTodayCell: UITableViewCell {

    static let nibName = "WorkListTodayCell"
    static let reuseId = "WorkListTodayCell"
    
    // MARK: - IBOutlets
    @IBOutlet weak var stateView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var optionsView: UIView!
    
    
    var viewModel: WorkListTodayCellViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.optionsView.layer.cornerRadius = 6.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Functions
    
    func setup(viewModel: WorkListTodayCellViewModel) {
        self.viewModel = viewModel
        self.setupUI()
    }
    
    private func setupUI() {
        guard let viewModel = self.viewModel else {
            return
        }
        
        descriptionLabel.text = viewModel.title
        hourLabel.text = viewModel.date
        stateView.image = viewModel.statusImage
        
        optionsView.backgroundColor = .red
    }

    
}
