//
//  WorkListTodayCellViewModel.swift
//  PairProgramming
//
//  Created by Natalia González Junco on 7/09/21.
//

import UIKit

class WorkListTodayCellViewModel {
    private let task: Task
    
    init(task: Task) {
        self.task = task
    }
    
    public var title: String {
        task.description
    }
    
    public var date: String {
        "\(task.initialDate)"
    }
    
    public var statusImage: UIImage {
        guard let image = UIImage(systemName: task.status.cellImageName) else {
            assertionFailure("\(task.status.cellImageName) Esta imagen deberia existir")
            return UIImage()
        }
        return image
    }
    
    public var statusColor: UIColor {
        guard let color = UIColor(named: task.status.cellStatusColorName) else {
            assertionFailure("Este color deberia existir")
            return UIColor()
        }
        return color
    }
    
}
