//
//  WorkTodayListCellFill.swift
//  WhateverList
//
//  Created by Natalia González Junco on 10/09/21.
//

import UIKit

class WorkTodayListCellFill: UITableViewCell {
    
    static let nibName = "WorkListTodayCellFill"
    static let reuseId = "WorkListTodayCellFill"
    
    
    @IBOutlet weak var inboxLabel: UILabel!
    @IBOutlet weak var numberTaskLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
